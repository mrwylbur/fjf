<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "DTD/xhtml1-transitional.dtd">
<html>
<head>
<title>Fred Jurewicz Financial Planning and Insurance Provider of Shakopee, MN </title>

<link href="fjfinancial.css" rel="stylesheet" type="text/css" />
</head>

<body>

<div id="frame">  

<?php 
include 'fjf-masthead.php';
include 'fjf-menu.php';
?>
<div id="contentcenter">

<h1>Financial Consulting Services</h1>

<img src="images/photos/Fred-CSA.jpg" alt="Fred Jurewicz" width="177" height="260" hspace="10" align="right" class="bordered">
		    
			<P>Sound planning in today's economy is not only important, but it can
		      be the critical difference in living a comfortable lifestyle, enjoying
		      retirement and leaving one's estate in proper order.</P>
		    <p>Since 1984 Fred Jurewicz has been helping school teachers, small business
		      owners and individuals who want to be prudent about planning their financial
		      future.</p>
		    <p><em>Fred Jurewicz Financial</em> has evolved into a business with a purpose:
		      to help people plan their financial lives with knowledge and strategy.
		      In today's world of complex investments, constantly changing tax laws
		      and volatile markets, financial planning can be difficult and confusing. </p>
		    <p>As a Registered Representative of&nbsp;<a href="http://www.workmansecurities.com">Workman
		        Securities Corporation</a>, Fred Jurewicz adheres to
		        the investment philosophy of: Diversification; Long-Term and
		        Professionally Managed.</p>
		    <p>In the fall of 2004, Fred earned the designation of Certified Senior
		      Advisor (CSA). The <a href="http://www.society-csa.com/">Society of Certified
		      Senior Advisors</a> is an educational
		      organization that provides an in depth certification program on senior
		      issues to professionals who work with seniors. </p>
		    <p>So if you are seeking a better understanding of your financial planning
		      and need the guidance of someone who can assist in this all-important
		      element of your life, give a&nbsp;call, send an&nbsp;e-mail&nbsp;or&nbsp;FAX.</p>
    <p><em>Fred Jurewicz Financial</em> could be the help that you have been seeking.</p>
			
<TABLE WIDTH="375" BORDER="1" align="center">
              <TR>
                <TD VALIGN="MIDDLE" ALIGN="CENTER"><br />
                  Securites offered through <br>
      Workman Securities Corporation<br>
      6500 City West Pkwy. Ste 350<br>
      Eden Prairie, MN 55344<br>
      (952) 541-6094</FONT><br />
      &nbsp;</TD>
              </TR>
    </TABLE>
		    <p>&nbsp;</p>
		    <p><em>Fred Jurewicz is a Registered Representative licensed to solicit
                  securities in Minnesota and Wisconsin and is insurance licensed
                  in Minnesota. The website is intended for informational purposes
                  and is not intended to be for the solicitation of residents of
              those states Fred Jurewicz is not licensed.</em></p>


 
 </div>

<?php include 'fjf-footer.php'; ?>

		
</div>

</body>
</html>
